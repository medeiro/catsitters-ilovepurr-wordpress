      <!-- section-->
      <section id="contacto" class="container-fluid">
         <!-- section heading -->  
         <div class="section-heading text-center">
            <p class="subtitle">Ponete en contacto</p>
            <h2>Escribinos</h2>
         </div>
         <!-- /section-heading -->  
         <div class="container">
            <div class="row">
               <!-- image -->
               <img  src="<?php echo get_stylesheet_directory_uri() ?>/img/contactbg1.png" class="img-fluid contact-home-img hidden-medium-small" alt="">
               <!-- contact box -->  
               <div class="col-lg-5 offset-lg-2 h-100">
                  <div class="contact-form3 bg-secondary"  data-aos="flip-right">
                     <div class="contact-image bg-secondary">
                        <!-- envelope icon-->
                        <i style="color:#eebab9" class="fas fa-envelope bg-secondary"></i>
                     </div>
                     <h4 class="text-center mt-3 text-light">Mensaje</h4>
                     <!-- 
                     <div id="contact_form">
                        <div class="form-group">
                           <div class="row">
                              
                              <div class="col-md-12">
                                 <label>Nombre<span class="required">*</span></label>
                                 <input type="text" name="NAME" class="form-control input-field" required=""> 
                              </div>
                              <div class="col-md-12">
                                 <label>Email<span class="required">*</span></label>
                                 <input type="email" name="EMAIL" class="form-control input-field" required=""> 
                              </div>
                              <div class="col-md-12">
                                 <label>Numero</label>
                                 <input type="text" name="NUMBER" class="form-control input-field"> 
                              </div>
                              <div class="col-md-12">
                                 <label>Mensaje<span class="required">*</span></label>
                                 <textarea  name="MESSAGE id="message" class="textarea-field form-control" rows="3"  required=""></textarea>
                              </div>
                           </div>
                                                                                                                       
                             <input type="submit" value="Enviar" class="btn btn-quaternary btn-block mt-3" />
                           </div>
                     
                     
                        <div id="contact_results"></div>
                     </div>
                    -->
                    <?php if ( is_active_sidebar( 'contact-form' ) ) : ?>
                     <div id="contact-form">
                       <?php dynamic_sidebar( 'contact-form' ); ?>
                    </div>
                    <?php endif; ?>
                  </div>
               </div>
               <!-- /col-lg-->
               <div class="text-center col-lg-5 res-margin">
                  <h3>Responderemos a la brevedad.</h3>
                  <p>Si tenes alguna duda o necesitas más información, no dudes en escribirnos.</p>
                  <!-- contact icons-->
                  <ul class="list-inline mt-3 list-contact colored-icons font-weight-bold">
                     <li class="list-inline-item"><i class="fa fa-envelope margin-icon"></i><a href="mailto:ilovepurrok@gmail.com">ilovepurrok@gmail.com</a></li>
                     <li class="list-inline-item"><i class="fa fa-phone margin-icon"></i>341 5972548</li>
                     <li class="list-inline-item" ><i class="fa fa-map-marker margin-icon"></i>Rosario, Santa Fe</li>
                  </ul>
                  <!-- /list-->
                  <!--divider-->
                  <hr class="mt-2">
                  <!-- map-->
                  <div id="map-canvas" class="mt-5"></div>
               </div>
            </div>
            <!-- /row-->
         </div>
         <!-- /container-->
      </section>
      <!-- /section -->
