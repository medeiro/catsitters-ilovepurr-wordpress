<section id="servicios" class="container">
         <div class="container">
            <!-- section heading -->  
            <div class="section-heading text-center">
               <p class="subtitle">Que ofrecemos?</p>
               <h2>Nuestro servicio</h2>
            </div>
            <!-- /section heading -->
            <div class="row">
               <div class="col-lg-7">
                  <h3> Servicio de calidad para tu gatito</h3>
                <!--  <span class="h7 mt-0"> Cuidamos a tu gatito mientras no estás </span<br><br>-->
                  <p class="mt-3">I love purr ofrece un servicio que incluye una visita de 50 minutos a tu domicilio. Durante este
                     tiempo, nos encargamos de cuidar y acompañar a tu gatito mientras estás fuera de casa.<br>
                      Durante la visita nos ocupamos de:
                       <ul>
                          <li type="disc">Limpiar sus bandejas sanitarias, de alimento y agua.</li>
                          <li type="disc">Servir su alimento según la porción solicitada.</li>
                          <li type="disc">Ordenar y/o limpiar cualquier travesura de los gatitos.</li>
                          <li type="disc">Los entretenemos con juguetes y actividades para asegurarnos de que tengan un tiempo
divertido y estimulante.</li>
                          <li type="disc">Los mimamos y les brindamos la contención y cariño que necesitan para sentirse cómodos y
seguros mientras están con nosotras.</li>
                       
                        </ul>
                        Para que estes al tanto de todo y puedas ver como estan y lo que estan haciendo, te enviaremos la
ubicación en tiempo real, fotos y videos a través de WhatsApp.
                     </p>
                                       <!-- Button -->	 
                  <a href="#contacto" class="btn btn-primary mt-3 mb-2">Contactanos</a>
                 
               </div>
               <!-- /col-lg-7-->
               <div class="col-lg-5">
                  <img style="margin-top:80px" src="<?php echo get_stylesheet_directory_uri() ?>/img/introimg1.jpg" class="img-fluid" alt="">
               </div>
               <!-- /col-lg-5-->   
            
            </div>
            <!-- /row -->
         </div>
         <!-- /container -->
      </section>