<section id="nosotras" class="overlay-light">
   <div class="container">
      <!-- section heading -->  
      <div class="section-heading text-center">
         <p class="subtitle">Conocenos</p>
         <h2>Quiénes somos?</h2>
      </div>
      <!-- /section-heading -->
      <div class="row">
         <!-- Tabs -->
         <div class="col-md-12">
            <!-- navigation -->
            <nav>
               <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active show" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab" aria-selected="true">Nosotras</a>
                     </div>
            </nav>
            <!-- tab-content -->
            <div class="tab-content block-padding" id="nav-tabContent">
               <div class="tab-pane  fade active show" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                  <!-- row -->
                  <div class="row">
                     <div class="col-lg-5 aos-init aos-animate" data-aos="zoom-out">
                        <!-- image -->
						<div class="img-zoom-hover">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/abouttab1.jpg" alt="" class="img-rotate-outline  img-left-absolute img-fluid">
                      </div>
					  <!-- /img-zoom-hover -->
					 </div>
					 <!-- /col-lg-5 -->
                     <div class="col-lg-7 res-margin">
                        <h3>Tu gatito en buenas manos!</h3>
                        <!--divider -->
                        <hr class="small-divider left">
                        <p>¡Hola! Soy Mica, una apasionada del mundo felino. Mi amor por los gatitos es tan
grande que he decidido crear &quot;I Love Purr&quot;. Pasar tiempo con ellos me hace sentir en
paz y disfruto mucho del contacto con estos animales. Además, siento que la compañía
es mutua y su ronroneo es la mayor muestra de amor que uno puede recibir.
</p>
                        <div class="accordion">
                           <!-- collapsible accordion 1 -->
                           <div class="card">
                              <div class="card-header">
                                 <a class="card-link collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                                    Objetivo
                                 </a>
                              </div>
                              <!-- /card-header -->
                              <div id="collapseOne" class="collapse show" data-parent=".accordion" style="">
                                 <div class="card-body">
                                    <p>Nuestro objetivo es proporcionarte la tranquilidad y la libertad de poder salir de tu hogar
durante el tiempo que necesites, sin tener que preocuparte por dejar a tu gato solo. Nos
encargaremos de cuidarlo responsable y amorosamente para que puedas confiar en nosotros y
tener la certeza de que está en buenas manos.</p>
                                 </div>
                              </div>
                           </div>
                           <!-- /card -->
                           <!-- collapsible accordion 2 -->
                         
                        </div>
                        <!-- /accordion -->
                        <!-- Button -->	 
                       
                     </div>
                     <!-- /col-lg-6-->
                  </div>
                  <!-- row -->
               </div>

            </div>
            <!-- ./Tab-content -->
         </div>
         <!-- col-md-12 -->
      </div>
      <!-- /row -->
   </div>
   <!-- /container-->
</section>