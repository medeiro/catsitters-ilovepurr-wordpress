<?php

add_theme_support( 'title-tag' ); 
add_action('wp_head', 'anadir_js');
add_action('wp_enqueue_scripts',"anadir_css");
add_theme_support( 'post-thumbnails' );

function anadir_css(){


    wp_enqueue_style('1','https://fonts.googleapis.com/css?family=Roboto:400,600,700%7CMontserrat:400,500,600,700');
    wp_enqueue_style('2',get_stylesheet_directory_uri().'/fonts/flaticon/flaticon.css');
    wp_enqueue_style('3',get_stylesheet_directory_uri().'/fonts/fontawesome/fontawesome-all.min.css');
    wp_enqueue_style('4',get_stylesheet_directory_uri().'/vendor/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('5',get_stylesheet_directory_uri().'/css/style.css');
    wp_enqueue_style('6',get_stylesheet_directory_uri().'/css/plugins.css');
    wp_enqueue_style('7',get_stylesheet_directory_uri().'/styles/maincolors.css');
    wp_enqueue_style('8',get_stylesheet_directory_uri().'/vendor/layerslider/css/layerslider.css');
 

        
}




function anadir_js() {

    wp_enqueue_script('1', get_stylesheet_directory_uri().'/vendor/jquery/jquery.min.js');
    wp_enqueue_script('2', get_stylesheet_directory_uri().'/vendor/bootstrap/js/bootstrap.min.js');
    wp_enqueue_script('3', get_stylesheet_directory_uri().'/js/custom.js');
    wp_enqueue_script('4', get_stylesheet_directory_uri().'/js/plugins.js');
    wp_enqueue_script('5', get_stylesheet_directory_uri().'/js/prefixfree.min.js');
    wp_enqueue_script('6', get_stylesheet_directory_uri().'/js/counter.js');
    wp_enqueue_script('7', get_stylesheet_directory_uri().'/js/map.js');
    wp_enqueue_script('8', get_stylesheet_directory_uri().'/vendor/layerslider/js/greensock.js');
    wp_enqueue_script('9', get_stylesheet_directory_uri().'/vendor/layerslider/js/layerslider.transitions.js');
    wp_enqueue_script('10', get_stylesheet_directory_uri().'/vendor/layerslider/js/layerslider.kreaturamedia.jquery.js');
    wp_enqueue_script('11', get_stylesheet_directory_uri().'/vendor/layerslider/js/layerslider.load.js');
  
}


add_action( 'widgets_init', 'register_new_sidebars' );
function register_new_sidebars() {

    register_sidebar(array(
    'name' => __('Contact form'),
    'id' => 'contact-form',
    'before_widget' => '<div class="contact-form">',
    'after_widget' => '</div>',
    //'before_title' => '<h4>',
    //'after_title' => '</h4>'
    ));
}