  <!-- ==== footer ==== -->
  <footer class="bg-light pattern1">
         <div class="container">
            <div class="row">
               <div class="col-lg-4 text-center ">
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo-footer.jpg"  class="logo-footer img-fluid" alt=""/>
                  <!-- Start Social Links -->
                  <ul class="social-list text-center list-inline">
                     <li class="list-inline-item"><a title="Facebook" href="https://www.facebook.com/catsitters.ilovepurr"><i class="fab fa-facebook-f"></i></a></li>
                     <li class="list-inline-item"><a  title="Instagram" href="https://www.instagram.com/catsitters.ilovepurr/"><i class="fab fa-instagram"></i></a></li>
                  </ul>
                  <!-- /End Social Links -->
               </div>
              
               <!--/ col-lg -->
               <div class="col-lg-4">
                  <h5>Servicio</h5>
                  <!--divider -->
                  <hr class="small-divider left"/>
                  <p class="mt-3">Brindamos un servicio que consta de una visita a tu domicilio, en la que cuidamos y acompañamos a tu gatito mientras estas fuera de casa.</p>
               </div>
               <!--/ col-lg -->
               <div class="col-lg-4">
                  <h5>Contactanos</h5>
                  <!--divider -->
                  <hr class="small-divider left"/>
                  <ul class="list-unstyled mt-3">
                     <li class="mb-1"><i class="fas fa-phone margin-icon "></i>341-5972548 </li>
                     <li class="mb-1"><i class="fas fa-envelope margin-icon"></i><a href="mailto:ilovepurrok@gmail.com">ilovepurrok@gmail.com</a></li>
                     <li><i class="fas fa-map-marker margin-icon"></i>Rosario, Santa Fe.</li>
                  </ul>
                  <!--/ul -->
               </div>
               <!--/ col-lg -->
               
               <!--/ col-lg -->
            </div>
            <!--/ row-->
            <hr/>
            <div class="row">
               <div class="credits col-sm-12">
                  <p>Todos los derechos reservados 2022</p>
               </div>
            </div>
            <!--/col-lg-12-->
         </div>
         <!--/ container -->
         <!-- Go To Top Link -->
         <div class="page-scroll hidden-sm hidden-xs">
            <a href="#top" class="back-to-top"><i class="fa fa-angle-up"></i></a>
         </div>
         <!--/page-scroll-->
      </footer>
      <!--/ footer-->
      <?php wp_footer(); ?>
    
   </body>
</html>
