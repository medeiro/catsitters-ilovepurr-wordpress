<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- page title -->
 
      <!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->

      <?php wp_head(); ?>
   </head>
   <!-- ==== body starts ==== -->
   <body id="top">
      <!-- Preloader  -->
      <div style="background-color:#55537d;" id="preloader">
         <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
               <div class="preloader-logo">
                  <!--logo -->
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png" alt="" class="img-fluid">
                  <!--preloader circle -->
                  <div class="lds-ring">
                     <div></div>
                     <div></div>
                     <div></div>
                     <div></div>
                  </div>
               </div>
               <!--/preloader logo -->
            </div>
            <!--/row -->
         </div>
         <!--/container -->
      </div>
      <!--/Preloader ends -->
    
      <nav id="main-nav" class="navbar-expand-xl fixed-top">
            <!-- Start Top Bar -->
            <div class="container-fluid top-bar" >
               <div class="container">
                  <div class="row">
                     <div class="col-md-12">
                        <!-- Start Contact Info -->
                        <ul class="contact-details float-left">
                           <li><i class="fa fa-map-marker"></i>Rosario, Santa Fe.</li>
                           <li><i class="fa fa-envelope"></i><a href="mailto:ilovepurrok@gmail.com">ilovepurrok@gmail.com</a></li>
                           <li><i class="fa fa-phone"></i>341 5972548</li>
                        </ul>
                        <!-- End Contact Info -->
                        <!-- Start Social Links -->
                        <ul class="social-list float-right list-inline">
                           <li class="list-inline-item"><a title="Facebook" href="https://www.facebook.com/catsitters.ilovepurr"><i class="fab fa-facebook-f"></i></a></li>
                           <li class="list-inline-item"><a  title="Instagram" href="https://www.instagram.com/catsitters.ilovepurr/"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                        <!-- /End Social Links -->
                     </div>
                     <!-- col-md-12 -->
                  </div>
                  <!-- /row -->
               </div>
               <!-- /container -->
            </div>
            <!-- End Top bar -->
            <!-- Navbar Starts -->
            <div class="navbar container-fluid">
               <div class="container ">
                  <!-- logo -->
                  <a class="nav-brand" href="#inicio">
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logo.png" alt="" class="img-fluid">
                  </a>
                  <!-- Navbartoggler -->
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggle-icon">
                  <i class="fas fa-bars"></i>
                  </span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarResponsive">
                     <ul class="navbar-nav ml-auto">
                        <!-- menu item -->
                        <li class="nav-item active">
                           <a class="nav-link" href="">Inicio
                           </a>
                        </li>
                        <li class="nav-item active">
                           <a class="nav-link" href="#servicios">Servicios
                           </a>
                        </li>
                        <li class="nav-item active">
                           <a class="nav-link" href="#nosotras">Nosotras
                           </a>
                        </li>
                     <!--   <li class="nav-item active">
                           <a class="nav-link" href="#adopta">Adopta
                           </a>
                        </li>-->
                        <li class="nav-item active">
                           <a class="nav-link" href="#contacto">Contacto
                           </a>
                        </li>
                     
                     </ul>
                     <!--/ul -->
                  </div>
                  <!--collapse -->
               </div>
               <!-- /container -->
            </div>
            <!-- /navbar -->
      </nav>
      <!-- /nav -->
   
    
