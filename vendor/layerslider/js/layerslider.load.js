$(document).ready(function () {
	"use strict";

	// Calling LayerSlider 
       var jsFileLocation = $('#11-js').attr('src');
       jsFileLocation = jsFileLocation.replace('/js/layerslider.load.js?ver=6.0.1', '');   
    
        $('#slider').layerSlider({
            responsive: true,
			fitScreenWidth:false,
            responsiveUnder: 1280,
            layersContainer: 1280,
            skin: 'outline',
			thumbnailNavigation: 'disabled',
            hoverPrevNext: true,
            skinsPath:  jsFileLocation+'/skins/',
            autoStart: true,
			autoPlayVideos : false
        });

		
 });