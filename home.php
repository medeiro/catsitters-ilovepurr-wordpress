<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>
   
         <!-- ==== Slider ==== -->
     <div id="slider" style="width:1200px;margin:0 auto;margin-bottom: 0px;">
		  <!-- Slide 1 -->
		  <div class="ls-slide" data-ls="duration:4000; transition2d:7;">
			 <!-- bg image  -->
			 <img src="<?php echo get_stylesheet_directory_uri() ?>/img/slider/slide1.jpg" class="ls-bg" alt="" />
			 <!-- text  -->
			 <div class="ls-l header-wrapper" data-ls="offsetxin:-50;durationin:1400;delayin:300;offsetxout:-200;durationout:1000;">
				<div class="header-text text-light">
				   <h1>Bienvenidos a<br><span> I love purr </span></h1>
				   <!--the div below is hidden on small screens  -->
				   <div class="hidden-small">
				      <p class="header-p">Cuidado y acompañamiento de gatitos a domicilio.</p>
					 
				   </div>
				  
				   <a class="btn btn-secondary btn-solicitar" href="#contacto">Solicitar presupuesto</a>
				   <!--/d-none  -->
				</div>
				<!-- header-text  -->
			 </div>
		  </div>
	
		
	
	   </div>
	   <!-- /slider -->
	   <?php get_template_part( 'parts/serviciosSection' ); ?>
	   <?php get_template_part( 'parts/nosotrasSection' ); ?>
       <?php get_template_part( 'parts/contactFormSection' ); ?>

      <?php
      get_footer();
